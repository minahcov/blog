getLogger = (module) ->
  path = module.filename.split("/").slice(-2).join("/") #отобразим метку с именем файла, который выводит сообщение
  new winston.Logger(transports: [new winston.transports.Console(
    colorize: true
    level: "debug"
    label: path
  )])

winston = require("winston")

module.exports = getLogger