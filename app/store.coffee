App = require 'app'

#App.ApplicationAdapter = DS.FixtureAdapter.extend()

App.ApplicationAdapter = DS.RESTAdapter.extend
  host: 'http://localhost:3000'
  namespace: 'blog'
  serializer: App.ApplicationSerializer

App.ApplicationSerializer = DS.RESTSerializer.extend
  # youre code here
  primaryKey: '_id'