App = require 'app'

App.BlogPostController = Ember.ObjectController.extend
  sinopsis: (()->
    str = "..."
    if (@get "description").length > 20
      (@get "description").substring(0, 20) + str
    else
      (@get "description").substring(0, 20)
  ).property()
