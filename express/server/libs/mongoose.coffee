mongoose = require("mongoose")
log = require("./log")(module)
config = require("./config")
#mongoose.connect "mongodb://localhost/blog"
mongoose.connect config.get('mongoose:uri')
db = mongoose.connection
db.on "error", (err) ->
  log.error "connection error:", err.message
  return

db.once "open", callback = ->
  log.info "Connected to DB!"
  return

Schema = mongoose.Schema

# Schemas
Post = new Schema(
  title:
    type: String
    required: true

  description:
    type: String
    required: true
)

# validation
#Post.path("title").validate (v) ->
 # v.length > 5 and v.length < 70

PostModel = mongoose.model("Post", Post)
module.exports.PostModel = PostModel