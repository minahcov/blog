# this file is auto-completed if you are using generators.
# Example:
# `scaffolt template user/index`
require 'templates/application'
require 'templates/index'
require 'templates/blog/posts'
require 'templates/blog/post'
require 'templates/blog/addPost'
require 'templates/blog/editPost'
