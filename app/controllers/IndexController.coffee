App = require 'app'

App.IndexController = Ember.ArrayController.extend
  actions:
  	deletePost: (post) ->
 	  if confirm('You want to delete post ' + post._data.title + '. A you sure?')
 	  	post.destroyRecord()
