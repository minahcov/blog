App = require 'app'

App.BlogPostsController = Ember.ArrayController.extend
  itemController: "blogPost" 
  actions:
    deletePost: (post) ->
        post.deleteRecord()
        post.save()