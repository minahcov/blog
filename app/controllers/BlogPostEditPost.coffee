App = require 'app'

App.BlogEditPostController = Ember.ObjectController.extend(
  actions:
    savePost: ->
      req = @get("model").save()

      @transitionToRoute "blog.posts"
      
      return
)